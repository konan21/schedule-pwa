/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {
    RECEIVE_DIRECTIONS,
    RECEIVE_DIRECTION_ITEMS,
    FAIL_DIRECTION_ITEMS,
    REQUEST_DIRECTION_ITEMS
} from '../actions/directions.js';
import { createSelector } from 'reselect';

const directions = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_DIRECTIONS:
            return {
                ...state,
                ...action.directions
            };
        case REQUEST_DIRECTION_ITEMS:
        case RECEIVE_DIRECTION_ITEMS:
        case FAIL_DIRECTION_ITEMS:
            const directionId = action.directionId;
            return {
                ...state,
                [directionId]: category(state[directionId], action)
            };
        default:
            return state;
    }
}

const direction = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_DIRECTION_ITEMS:
            return {
                ...state,
                failure: false,
                isFetching: true
            };
        case RECEIVE_DIRECTION_ITEMS:
            return {
                ...state,
                failure: false,
                isFetching: false,
                items: {
                    ...action.items.reduce((obj, item) => {
                        obj[item.name] = item;
                        return obj;
                    }, {})
                }
            };
        case FAIL_DIRECTION_ITEMS:
            return {
                ...state,
                failure: true,
                isFetching: false
            };
        default:
            return state;
    }
}

export default directions;

const directionsSelector = state => state.directions;

const directionNameSelector = state => state.app.directionName;

export const currentDirectionSelector = createSelector(
    directionsSelector,
    directionNameSelector,
    (directions, directionName) => (directions && directionName ? directions[directionName]: null)
);

const itemNameSelector = state => state.app.itemName;

export const currentItemSelector = createSelector(
    currentDirectionSelector,
    itemNameSelector,
    (direction, itemName) => (direction && direction.items && itemName ? direction.items[itemName] : null)
);