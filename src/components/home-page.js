/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class HomePage extends PageViewElement {
  static get styles() {
    return [
      SharedStyles,
      css `
        .s-hero {
          position: relative;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
          -ms-flex-direction: column;
          flex-direction: column;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          justify-content: center;
          min-height: 440px;
          margin-bottom: -100px;
          z-index: 3;
        }
        
        @media (max-width: 768px) {
          .s-hero {
            min-height: 340px;
            margin-bottom: 0;
          }
        }
        
        .s-hero__title {
          font-size: 56px;
          font-family: 'Kurale', sans-serif;
          color: #fff;
          text-transform: uppercase;
          line-height: 1;
          text-align: center;
          padding: 20px 0;
          position: relative;
          z-index: 3;
        }
        
        @media (max-width: 576px) {
          .s-hero__title {
            font-size: 42px;
          }
        }
        
        @media (max-width: 425px) {
          .s-hero__title {
            font-size: 36px;
          }
        }
        
        .s-hero__title .s-hero__caption {
          font-size: 50%;
          color: hsla(0, 0%, 100%, .7);
          display: block;
        }
        
        @media (max-width: 576px) {
          .s-hero__title .s-hero__caption {
            font-size: 66.66667%;
          }
        }
        
        .s-hero__background {
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          background-image: url("../../images/bg-poltava-680w.jpg");
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
          z-index: 1;
        }
        
        .s-hero__background:after {
          content: "";
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          background-color: rgba(0, 0, 0, .4);
          z-index: 2;
        }
        
        .bg-pic {
          display: block;
          width: 100%;
          height: 100%;
          -o-object-fit: cover;
          object-fit: cover;
          -o-object-position: top;
          object-position: top;
        }
        
        .s-schedule {
          position: relative;
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
          grid-gap: 40px;
          z-index: 5;
        }
        
        .table {
          width: 100%;
          max-width: 100%;
          margin-bottom: 35px;
          background-color: transparent;
          border-spacing: 0;
          border-collapse: collapse;
        }
        
        .table .table__body tr:hover {
          background-color: var(--app-light-color);
        }
        
        .table td, .table th {
          font-size: 14px;
          line-height: 1.42857;
          padding: 8px 0;
          vertical-align: middle;
        }
        
        .table__header td, .table__header th {
          border-bottom: 2px solid #ddd;
          text-align: center;
        }
        
        .table__body td {
          text-align: center;
        }
        
        .table__body tr:not(:last-of-type) td {
          border-bottom: 1px solid #ddd;
        }
      `
    ];
  }

  render() {
    return html`
    <section class="s-hero">
      <div class="s-hero__background"></div>

      <h1 class="s-hero__title">
        <span class="s-hero__caption">Розклад</span> Супрунівка
      </h1>
    </section>

    <section class="s-schedule container">
      <article class="s-schedule__article s-schedule__article--1 box">
        <header class="s-book__header">
          <h2 class="page-title">Половки</h2>
        </header>

        <div class="s-book__main">
          <table class="table table--hover">
            <thead class="table__header">
            <tr>
              <th>Супрунівка</th>
              <th>Ринок</th>
            </tr>
            </thead>
            <tbody class="table__body">
            <tr>
              <td>6:20</td>
              <td>7:00</td>
            </tr>
            <tr>
              <td>6:45</td>
              <td>7:25</td>
            </tr>
            <tr>
              <td>7:25</td>
              <td>8:20</td>
            </tr>
            <tr>
              <td>8:00</td>
              <td>8:45</td>
            </tr>
            <tr>
              <td>8:50</td>
              <td>9:45</td>
            </tr>
            <tr>
              <td>10:15</td>
              <td>-</td>
            </tr>
            <tr>
              <td>9:20</td>
              <td>10:05</td>
            </tr>
            <tr>
              <td>10:40</td>
              <td>11:20</td>
            </tr>
            <tr>
              <td>11:55</td>
              <td>12:45</td>
            </tr>
            <tr>
              <td>-</td>
              <td>12:25</td>
            </tr>
            <tr>
              <td>12:50</td>
              <td>13:45</td>
            </tr>
            <tr>
              <td>14:20</td>
              <td>15:05</td>
            </tr>
            <tr>
              <td>15:40</td>
              <td>-</td>
            </tr>
            <tr>
              <td>-</td>
              <td>14:35</td>
            </tr>
            <tr>
              <td>15:10</td>
              <td>16:05</td>
            </tr>
            <tr>
              <td>16:40</td>
              <td>17:25</td>
            </tr>
            <tr>
              <td>-</td>
              <td>17:00</td>
            </tr>
            <tr>
              <td>17:30</td>
              <td>18:25</td>
            </tr>
            <tr>
              <td>18:00</td>
              <td>18:45</td>
            </tr>
            <tr>
              <td>19:20</td>
              <td>20:05</td>
            </tr>
            <tr>
              <td>20:35</td>
              <td>до Половок</td>
            </tr>
            </tbody>
          </table>
        </div>
      </article>
      
      <article class="s-schedule__article s-schedule__article--2 box">
        <header class="s-book__header">
          <h2 class="page-title">Алмазний</h2>
        </header>

        <div class="s-book__main">
          <table class="table table--hover">
            <thead class="table__header">
            <tr>
              <th>Супрунівка</th>
              <th>Ринок</th>
            </tr>
            </thead>
            <tbody class="table__body">
            <tr>
              <td>6:05</td>
              <td>6:45</td>
            </tr>
            <tr>
              <td>7:00</td>
              <td>7:45</td>
            </tr>
            <tr>
              <td>7:15</td>
              <td>8:05</td>
            </tr>
            <tr>
              <td>8:20</td>
              <td>9:05</td>
            </tr>
            <tr>
              <td>8:40</td>
              <td>9:25</td>
            </tr>
            <tr>
              <td>9:40</td>
              <td>11:30</td>
            </tr>
            <tr>
              <td>10:00</td>
              <td>10:45</td>
            </tr>
            <tr>
              <td>11:15</td>
              <td>-</td>
            </tr>
            <tr>
              <td>12:10</td>
              <td>12:55</td>
            </tr>
            <tr>
              <td>13:30</td>
              <td>14:15</td>
            </tr>
            <tr>
              <td>-</td>
              <td>13:55</td>
            </tr>
            <tr>
              <td>14:30</td>
              <td>15:15</td>
            </tr>
            <tr>
              <td>14:50</td>
              <td>15:55</td>
            </tr>
            <tr>
              <td>15:50</td>
              <td>16:40</td>
            </tr>
            <tr>
              <td>16:30</td>
              <td>17:15</td>
            </tr>
            <tr>
              <td>17:50</td>
              <td>до Автовокзала</td>
            </tr>
            <tr>
              <td>17:15</td>
              <td>18:05</td>
            </tr>
            <tr>
              <td>17:50</td>
              <td></td>
            </tr>
            <tr>
              <td>18:35</td>
              <td>19:15</td>
            </tr>
            <tr>
              <td>19:45</td>
              <td>20:20</td>
            </tr>
            <tr>
              <td>20:55</td>
              <td>21:35</td>
            </tr>
            </tbody>
          </table>
        </div>
      </article>
      
      <article class="s-schedule__article s-schedule__article--3 box">
        <header class="s-book__header">
          <h2 class="page-title">Алмазний <span>(Шостаки)</span></h2>
        </header>

        <div class="s-book__main">
          <table class="table table--hover">
            <thead class="table__header">
            <tr>
              <th>Шостаки</th>
              <th>Ринок</th>
            </tr>
            </thead>
            <tbody class="table__body">
            <tr>
              <td>6:25</td>
              <td>7:10</td>
            </tr>
            <tr>
              <td>7:40</td>
              <td>8:25</td>
            </tr>
            <tr>
              <td>9:05</td>
              <td>10:15</td>
            </tr>
            <tr>
              <td>10:50</td>
              <td>11:45</td>
            </tr>
            <tr>
              <td>12:25</td>
              <td>13:15</td>
            </tr>
            <tr>
              <td>13:45</td>
              <td>14:45</td>
            </tr>
            <tr>
              <td>15:20</td>
              <td>16:15</td>
            </tr>
            <tr>
              <td>16:50</td>
              <td>17:35</td>
            </tr>
            <tr>
              <td>18:10</td>
              <td>18:55</td>
            </tr>
            <tr>
              <td>19:30</td>
              <td>20:45</td>
            </tr>
            <tr>
              <td>21:10</td>
              <td>до Автовокзала</td>
            </tr>
            </tbody>
          </table>
        </div>
      </article>
      
      <article class="s-schedule__article s-schedule__article--4 box">
        <header class="s-book__header">
          <h2 class="page-title">ДБК №16</h2>
          <p class="text-center" style="font-size: 14px; margin-bottom: 20px;">
            У вихідні та святкові дні ВІДСУТНІ наступні рейси:<br>
            5:50, 13:50, 14:30, 15:10, 19:15
          </p>
        </header>

        <div class="s-book__main">
          <table class="table table--hover">
            <thead class="table__header">
            <tr>
              <th>Половки</th>
              <th>ДБК</th>
            </tr>
            </thead>
            <tbody class="table__body">
            <tr>
              <td>5:50</td>
              <td>6:05</td>
            </tr>
            <tr>
              <td>6:20</td>
              <td>6:45</td>
            </tr>
            <tr>
              <td>7:00</td>
              <td>7:20</td>
            </tr>
            <tr>
              <td>7:40</td>
              <td>8:00</td>
            </tr>
            <tr>
              <td>8:20</td>
              <td>8:40</td>
            </tr>
            <tr>
              <td>9:00</td>
              <td>9:20</td>
            </tr>
            <tr>
              <td>9:40</td>
              <td>10:00</td>
            </tr>
            <tr>
              <td colspan="2">Перерва</td>
            </tr>
            <tr>
              <td>13:50</td>
              <td>14:10</td>
            </tr>
            <tr>
              <td>14:30</td>
              <td>14:50</td>
            </tr>
            <tr>
              <td>15:10</td>
              <td>15:30</td>
            </tr>
            <tr>
              <td>15:50</td>
              <td>16:10</td>
            </tr>
            <tr>
              <td>16:30</td>
              <td>16:50</td>
            </tr>
            <tr>
              <td>17:15</td>
              <td>17:30</td>
            </tr>
            <tr>
              <td>17:50</td>
              <td>18:10</td>
            </tr>
            <tr>
              <td>18:30</td>
              <td>18:50</td>
            </tr>
            <tr>
              <td>19:15</td>
              <td>19:30</td>
            </tr>
            </tbody>
          </table>
        </div>
      </article>
      
      <article class="s-schedule__article s-schedule__article--5 box">
        <header class="s-book__header">
          <h2 class="page-title">Садова</h2>
          <p class="text-center" style="font-size: 14px; margin-bottom: 20px;">
            Через Зигіна та Браїлки
          </p>
        </header>

        <div class="s-book__main">
          <table class="table table--hover">
            <thead class="table__header">
            <tr>
              <th>Супрунівка</th>
              <th>Ринок</th>
            </tr>
            </thead>
            <tbody class="table__body">
            <tr>
              <td>6:50</td>
              <td>7:30</td>
            </tr>
            <tr>
              <td>8:15</td>
              <td>8:50</td>
            </tr>
            <tr>
              <td>9:35</td>
              <td>-</td>
            </tr>
            <tr>
              <td>-</td>
              <td>12:02</td>
            </tr>
            <tr>
              <td>12:40</td>
              <td>-</td>
            </tr>
            <tr>
              <td>-</td>
              <td>16:20</td>
            </tr>
            <tr>
              <td>17:05</td>
              <td>17:40</td>
            </tr>
            <tr>
              <td>18:25</td>
              <td>-</td>
            </tr>
            </tbody>
          </table>
        </div>
      </article>
        
    </section>
    `;
  }
}

window.customElements.define('home-page', HomePage);
