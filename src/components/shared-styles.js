/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { css } from 'lit-element';

export const SharedStyles = css`
  :host {
    display: block;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    font-size: var(--app-font-size);
    color: var(--app-primary-color);
  }
        
  *, :after, :before {
    -webkit-box-sizing: inherit;
    box-sizing: inherit
  }
  
  h1, h2, h3, h4, h5, h6 {
    margin: 0;
    padding: 0;
    line-height: var(--app-line-height);
  }
  
  a:link, a:visited {
    color: var(--app-primary-color);
    text-decoration: none
  }
  
  li, ul {
    list-style: none
  }

  li, p, ul {
    margin: 0;
    padding: 0
  }
  
  .container {
    width: 100%;
    max-width: calc(100% - 120px);
    margin: 0 auto
  }
    
  @media (max-width: 768px) {
    .container {
      max-width: 100%
    }
  }
    
  .box {
    padding: 35px 40px;
    border-radius: 6px;
    -webkit-box-shadow: 0 2px 0 0 #e5e5e5;
    box-shadow: 0 2px 0 0 #e5e5e5;
    background-color: #fff
  }
    
  @media (max-width: 840px) {
    .box {
      padding: 35px 30px
    }
  }
    
  @media (max-width: 320px) {
    .box {
      padding: 35px 15px
    }
  }
    
  .page-title {
    font-size: 30px;
    text-align: center;
    font-weight: 300;
    margin-bottom: 20px
  }
  
  @media (max-width: 425px) {
    .page-title {
      font-size: 26px;
      margin-bottom: 12px
    }
  }
  
  .page-title span {
    font-size: 75%;
  }
  
  .text-center {
    text-align: center;
  }
  
  .have-not td {
    text-decoration: line-through;
    color: #cccccc;
  }
`;
