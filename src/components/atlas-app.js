/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html, css } from 'lit-element';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { installMediaQueryWatcher } from 'pwa-helpers/media-query.js';
import { installOfflineWatcher } from 'pwa-helpers/network.js';
import { installRouter } from 'pwa-helpers/router.js';
import { updateMetadata } from 'pwa-helpers/metadata.js';

// This element is connected to the Redux store.
import { store } from '../store.js';

// These are the actions needed by this element.
import {
  navigate,
  updateOffline,
  // updateDrawerState
} from '../actions/app.js';

// These are the elements needed by this element.
// import '@polymer/app-layout/app-drawer/app-drawer.js';
// import '@polymer/app-layout/app-header/app-header.js';
// import '@polymer/app-layout/app-scroll-effects/effects/waterfall.js';
// import '@polymer/app-layout/app-toolbar/app-toolbar.js';
// import { menuIcon } from './my-icons.js';
import './snack-bar.js';
// import './currentYear.js';

class AtlasApp extends connect(store)(LitElement) {
  static get properties() {
    return {
      appTitle: { type: String },
      _page: { type: String },
      // _drawerOpened: { type: Boolean },
      _snackbarOpened: { type: Boolean },
      _offline: { type: Boolean }
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: block;
          
          --app-font-size: 16px;
          
          --app-primary-color: #464646;
          --app-primary-secondary-color: #676767;
          --app-light-color: #f5f5f5;
          --app-light-secondary-color: #e5e5e5;
          --app-white-color: #ffffff;
          --app-dark-color: #000000;
          --app-accent-color: #88ce36;
          --app-accent-secondary-color: #368887;
        }

        /* Workaround for IE11 displaying <main> as inline */
        main {
          display: block;
          background-color: var(--app-light-color);
        }

        .page {
          display: none;
        }

        .page[active] {
          display: block;
        }
      `
    ];
  }

  render() {
    // Anything that's related to rendering should be done in here.
    return html`
      <!-- Main content -->
      <main role="main" class="main-content">
        <home-page class="page" ?active="${this._page === 'home'}"></home-page>
        <error404-page class="page" ?active="${this._page === 'error404'}"></error404-page>
      </main>

      <snack-bar ?active="${this._snackbarOpened}">
        Вы сейчас ${this._offline ? 'offline' : 'online'}.
      </snack-bar>
    `;
  }

  constructor() {
    super();
    // To force all event listeners for gestures to be passive.
    // See https://www.polymer-project.org/3.0/docs/devguide/settings#setting-passive-touch-gestures
    setPassiveTouchGestures(true);
  }

  firstUpdated() {
    installRouter((location) => store.dispatch(navigate(decodeURIComponent(location.pathname))));
    installOfflineWatcher((offline) => store.dispatch(updateOffline(offline)));
    // installMediaQueryWatcher(`(min-width: 460px)`,
    //     () => store.dispatch(updateDrawerState(false)));
  }

  updated(changedProps) {
    if (changedProps.has('_page')) {
      // const pageTitle = this.appTitle + ' - ' + this._page;
      const pageTitle = this.appTitle;
      updateMetadata({
        title: pageTitle,
        description: pageTitle
        // This object also takes an image property, that points to an img src.
      });
    }
  }

  stateChanged(state) {
    this._page = state.app.page;
    this._offline = state.app.offline;
    this._snackbarOpened = state.app.snackbarOpened;
    // this._drawerOpened = state.app.drawerOpened;
  }
}

window.customElements.define('atlas-app', AtlasApp);
