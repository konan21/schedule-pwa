/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class Error404Page extends PageViewElement {
  static get styles() {
    return [
      SharedStyles,
      css `
        .s-error404 {
          height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        a:link, a:visited, .link {
          text-decoration: underline;
          color: var(--app-accent-secondary-color);
        }
        a:hover, a.link:hover {
          text-decoration: none;
        }
      `
    ];
  }

  render() {
    return html`
      <section class="s-error404">
        <h2 class="page-title">Сторінка не знайдена! Помилка 404</h2>
        <p class="text-center">
          Сторінка на яку ви перейшли не знайдена. Поверніться назад на <a class="link" href="/">головну</a>
        </p>
      </section>
    `
  }
}

window.customElements.define('error404-page', Error404Page);
